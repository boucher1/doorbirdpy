Contributions are welcome, including bug fixes and new features within
scope, and especially updates to ensure compatibility with new DoorBird
API versions. 

# Code style

Line length: 120 characters
